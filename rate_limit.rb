require 'time'

class RateLimit
	def initialize(time, limit)
		@reset_on = Time.now + time
		@limit = limit
		@time = time
		@keys = {}
	end

	def increment(key)
		reset if Time.now >= @reset_on

		if @keys[key] == nil
			@keys[key] = 1
		else
			@keys[key] += 1
		end
		return @keys[key] >= @limit
	end

	def current_level(key)
		reset if Time.now >= @reset_on
		return @keys[key]
	end

	def is_limited?(key)
		reset if Time.now >= @reset_on
		return @keys[key] != nil && @keys[key] >= @limit
	end

	def reset
		@reset_on = Time.now + @time
		@keys = {}
	end
end
