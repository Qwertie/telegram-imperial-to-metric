require 'telegram/bot'
require 'yaml'
require './rate_limit'

Config = YAML.load_file('config.yml')
rate_limit = RateLimit.new(Config['rate_limit_time'], Config['rate_limit_limit'])
Telegram::Bot::Client.run(Config['api_key']) do |bot|
	bot.listen do |message|
		unless rate_limit.is_limited?(message.chat.id)
			if message.text =~ /(.*?\s|^)\d+ feet\s*.*/
				ammount = message.text.match(/(\d+) feet/).captures.first.to_f
				bot.api.send_message(
					chat_id: message.chat.id,
					text: "* #{message.text.gsub(/\d+ feet/, "#{ammount / 3.28084} metres")}"
				)
				rate_limit.increment(message.chat.id)
			elsif message.text =~ /(.*?\s|^)\d+ inches\s*.*/
				ammount = message.text.match(/(\d+) inches/).captures.first.to_f
				bot.api.send_message(
					chat_id: message.chat.id,
					text: "* #{message.text.gsub(/\d+ inches/, "#{ammount * 2.54} centimetres")}"
				)
				rate_limit.increment(message.chat.id)
			elsif message.text =~ /(.*?\s|^)\d+ miles\s*.*/
				ammount = message.text.match(/(\d+) miles/).captures.first.to_f
				bot.api.send_message(
					chat_id: message.chat.id,
					text: "* #{message.text.gsub(/\d+ miles/, "#{ammount * 1.60934} kilometres")}"
				)
				rate_limit.increment(message.chat.id)
			elsif message.text =~ /.*?(\d+ feet|d+ foot|\d+ inch|\d+ yard|ounce|miles).*/
				bot.api.send_message(chat_id: message.chat.id, text: "This is a Metric server, no Imperial")
				rate_limit.increment(message.chat.id)
			end
		end
	end
end
